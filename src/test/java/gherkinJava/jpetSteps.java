package gherkinJava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetSteps {
	
	WebDriver driver;



@Given("un navigateur est ouvert")
public void un_navigateur_est_ouvert() {
	System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
	driver = new FirefoxDriver();
    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
}

@When("je suis sur url")
public void je_suis_sur_url() {
	driver.get("https://petstore.octoperf.com/actions/Catalog.action");
}

@When("je clique sur le lien de connexion")
public void je_clique_sur_le_lien_de_connexion() {
	 driver.findElement(By.xpath("//*[@id=\"MenuContent\"]/a[2]")).click();
}

@When("je rentre le Username {string}")
public void je_rentre_le_Username(String username) {
	driver.findElement(By.xpath("//*[@name=\"username\"]")).clear();
	 driver.findElement(By.xpath("//*[@name=\"username\"]")).sendKeys(username);
}

@When("je rentre le password {string}")
public void je_rentre_le_password(String string) {
	driver.findElement(By.xpath("//*[@name=\"password\"]")).clear();
	driver.findElement(By.xpath("//*[@name=\"password\"]")).sendKeys(string);
}

@When("je clique sur login")
public void je_clique_sur_login() {
	 driver.findElement(By.xpath("//*[@id=\"Catalog\"]/form/input")).click();
}

@Then("utilisateur ABC est connecte")
public void utilisateur_ABC_est_connecte() {
    assertEquals("Sign Out", driver.findElement(By.xpath("//*[@id=\"MenuContent\"]/a[2]")).getText());
}

@Then("je peux lire le message acceuil {string}")
public void je_peux_lire_le_message_acceuil(String string) {
	 assertEquals(string, driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"]")).getText());
}
}