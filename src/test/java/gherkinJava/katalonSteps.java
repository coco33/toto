package gherkinJava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class katalonSteps {
	
	WebDriver driver;

	@Given("je suis connecte sur la page de prise de rendez-vous")
	public void je_suis_connect_sur_la_page_de_prise_de_rendez_vous() {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://katalon-demo-cura.herokuapp.com/");
	    driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();
	    driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
		 driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
		 driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();
	}

	@When("je renseigne les informations obligatoires")
	public void je_renseigne_les_informations_obligatoires() {
		driver.findElement(By.xpath("//*[@id=\"txt_visit_date\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"txt_visit_date\"]")).sendKeys("21/07/2022");
	   
	}

	@When("je clique sur Book Appointment")
	public void je_clique_sur_Book_Appointment() {
		driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]")).click();
	}

	@Then("Le rendez-vous est confirme")
	public void le_rendez_vous_est_confirm() {
		 assertEquals("Appointment Confirmation", driver.findElement(By.xpath("//*[@id=\"summary\"]/div/div/div[1]/h2")).getText());
	}

}