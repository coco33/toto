Feature: Connexion � app jpetstore avec data

  Scenario Outline: Connexion avec data
    Given un navigateur est ouvert
    When je suis sur url
    And je clique sur le lien de connexion
    And je rentre le Username <username>
    And je rentre le password <password>
    And je clique sur login
    Then utilisateur est connecte
    And je peux lire le message acceuil <nom>

    Examples: 
      | username   | password   | nom   |
      | "j2ee"     | "j2ee"     | "ABC" |
      | "ACID"     | "ACID"     | "ABC" |
